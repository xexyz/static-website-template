<?php

$isSecure = false;
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $isSecure = true;
}
elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
    $isSecure = true;
}

if(!$isSecure) {
	$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	header("Location:$redirect");
}

require_once 'Zend/Mail/Transport/Smtp.php';
require_once 'Zend/Mail.php';
require_once 'Google/autoload.php';

$show_suc = false;
$show_name = false;
$show_email1 = false;
$show_email2 = false;
$show_mess = false;

if(isset($_POST['email'])) {
    if(!isset($_POST['name']) || !isset($_POST['message'])) {
		//do something? i dunno
    }

    $name = $_POST['name'];
	if (strlen($name) == 0
		|| trim($name) == '')
		$show_name = true;
    $email_from = $_POST['email'];
	if (strlen($email_from) == 0
		|| trim($email_from) == '')
		$show_email1 = true;
    $message = $_POST['message'];
	if (strlen($message) == 0
		|| trim($message) == '')
		$show_mess = true;


    $email_exp = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
	if(!$show_email1 && !preg_match($email_exp,$email_from))
		$show_email2 = true;

	if ($show_name == false
		&& $show_email1 == false
		&& $show_email2 == false
		&& $show_mess == false)
	{
		$show_suc = true;

		$client = new Google_Client();
		$client->setScopes(array("https://mail.google.com/"));
		$client->setRedirectUri("urn:ietf:wg:oauth:2.0:oob");
		$client->setClientId('CLIENTID');
		$client->setClientSecret('CLIENTSECRET');
		$client->refreshToken('REFRESHTOKEN');
		$accessToken = $client->getAccessToken();
		$atjson = json_decode($accessToken, true);

		$email = 'no-reply@COMAPNYNAME.com';
		$token = $atjson['access_token'];
		$initClientRequestEncoded = base64_encode("user={$email}\1auth=Bearer {$token}\1\1");
		$config = array('ssl' => 'ssl', 'port' => '465', 'auth' => 'oauth2', 'xoauth2_request' => $initClientRequestEncoded);
		$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

		//reply to sender
		$reply_subject = "Thank you for contacting COMPANY NAME";
		$reply_message = "COMPANY NAME has received your message and will respond.  If this request was not sent by you, please disregard this email.";

		$reply_html = <<<EOT
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<body style='text-align:center;'>
<table cellspacing='0' cellpadding='0' style=max-width='640'><tr><td style='background-color:#131629;'>
<img alt='Logo'= src='../assets/logoemail.png' style='margin-left:0px; display:inline;=max-width:100%;padding:6px;' width='354px'>
</td></tr><tr><td><p>
Hello $name, COMPANY NAME has received your message and will respond.  If this request was not sent by you, please disregard this email.
</p></td></tr></table>
</body>
</html>
EOT;

		$reply = new Zend_Mail();
		$reply->setBodyText($reply_message);
		$reply->setBodyHtml($reply_html);
		$reply->setFrom($email, "COMPANY NAME" );
		$reply->addTo($email_from, $name);
		$reply->setSubject($reply_subject);
		$reply->send($transport);

		//forward
		$email_to = "webcontact@COMPANYNAME.com";
		$email_subject = "Website Contact";

		$email_message = "Form details below.\n\n";
		function clean_string($string) {
			$bad = array("content-type","bcc:","to:","cc:","href");
			return str_replace($bad,"",$string);
		}

		$email_message .= "Name: ".clean_string($name)."\n";
		$email_message .= "Email: ".clean_string($email_from)."\n";
		$email_message .= "Message: ".clean_string($message)."\n";

		$mail = new Zend_Mail();
		$mail->setBodyText($email_message);
		$mail->setFrom($email_from, $name);
		$mail->setReplyTo($email_from, $name);
		$mail->addTo($email_to, "Web Contact");
		$mail->setSubject($email_subject);
		$mail->send($transport);
	}
}
?>
<!doctype html>
	<head>
		<meta charset="utf-8" />
		<title>Contact -- COMPANY NAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME - Contact"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../assets/lettertype.png" />
		<meta property="og:url" content="../contact/" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<link rel="shortcut icon" href="../public/img/favicon.ico"/>
		<link rel="image_src" href="../assets/lettertype.png" />
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="../public/css/styles.css"/>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	</head>

	<body class="contact">
		<div class="cover">
		<div class="mask">
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="navbar-header">
				<a class="brand" href="../"><img src="../public/img/lettertype.svg"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../PAGE1/">PAGE1</a></li>
					<li><a href="../PAGE2/">PAGE2</a></li>
					<li><a href="../PAGE3/">PAGE3</a></li>
					<li class="active"><a href="../contact/">CONTACT</a></li>
					<li><a href="https://LOGIN/">LOGIN</a></li>
				</ul>
			</div>
		</nav>

		<div class="container-fluid">

		    <div class="row">
		        <div class="col-lg-6 col-md-6"></div>
		        <div class="col-lg-5 col-md-6">
		            <div class="section textdark">
						<h1 class="headline"> Contact Us </h1>
						<hr />
						<ul class="fa-ul">
							<li><h3><i class="fa-li fa fa-envelope-o"> </i>Send us a message:</h3></li>
						</ul>
						<?php if ($show_suc) { echo "<div id='success' class='well well-success'>Message sent successfully.</div>"; }?>
						<form style="padding-top:25px;" method="POST" class="form-horizontal" role="form" name="contact" action="../contact/">
							<div class="form-group danger">
								<label class="control-label col-sm-2" for="name">Name:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control danger" name="name" id="name" placeholder="Enter name" <?php if (!$show_suc && !$show_name) { echo "value='".$name."'"; } ?> >
									<?php if ($show_name) { echo "<p id='name-help' class='help-block alert-help'> Name is required. </p>"; }?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="email">Email:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="email" id="email" placeholder="Enter email" <?php if (!$show_suc && !$show_email1) { echo "value='".$email_from."'"; } ?> >
									<?php if ($show_email1) { echo "<p id='email-help' class='help-block alert-help'> Email is required. </p>	"; }?>
									<?php if ($show_email2) { echo "<p id='email-help2' class='help-block alert-help'> Email must have proper form. </p>	"; }?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="message">Message:</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="message" id="message" rows="12"><?php if (!$show_suc && !$show_mess) { echo $message; } ?> </textarea>
									<?php if ($show_mess) { echo "<p id='mess-help' class='help-block alert-help'> Message is required. </p>	"; }?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 ">
									<button type="submit" class="btn btn-default  pull-right">Send</button>
								</div>
							</div>
						</form>
		            </div>
		        </div>
		        <div class="col-lg-1 hidden-md"></div>
		    </div>
		</div>
		<footer class="cfooter">
			<p>
				<a href="../jobs/">Jobs</a>
				<a href="../legal/">Legal</a>
				<a href="../privacy/">Privacy</a>
			</p>
		</footer>
		</div>
		</div>
	</body>
	<script>
		var img = new Image();
		img.onload = function() {
			$(".mask").addClass("clear");
		};
		img.src = "../public/img/COVERPAGE4.jpg";

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</html>
