<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Jobs -- COMPANY NAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME - Jobs"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../assets/lettertype.png" />
		<meta property="og:url" content="../legal/" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="robots" content="NOINDEX, NOFOLLOW" />
		<link rel="shortcut icon" href="../public/img/favicon.ico"/>
		<link rel="image_src" href="../assets/lettertype.png" />
		<link rel="stylesheet" href="../public/css/styles.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	</head>
	<body class="jobs">
	<div class="cover">
	<div class="mask">
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="navbar-header">
				<a class="brand" href="../"><img src="../public/img/lettertype.svg"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../PAGE1/">PAGE1</a></li>
					<li><a href="../PAGE2/">PAGE2</a></li>
					<li><a href="../PAGE3/">PAGE3</a></li>
					<li><a href="../contact/">CONTACT</a></li>
					<li><a href="https://LOGIN/">LOGIN</a></li>
				</ul>
			</div>
		</nav>

		<div class="container-fluid">
		    <div class="row">
			    <div class="col-lg-1 col-md-1 hidden-sm"></div>
		        <div class="col-lg-4 col-md-5 col-sm-7">
		            <div class="section textgray">
			           <h1 class="headline"> Jobs </h1>
					   <p><i class="fa fa-user-plus"> </i> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
						 <ul class="fa-ul">
							 <li><i class="fa-li fa fa-half fa-circle"></i> Item 1</li>
							 <li><i class="fa-li fa fa-half fa-circle"></i> Item 2</li>
							 <li><i class="fa-li fa fa-half fa-circle"></i> Item 3</li>
						 </ul>
						 <p> Benefits include:</p>
						 <ul class="fa-ul">
							 <li><i class="fa-li fa fa-half fa-circle"></i> Item 1</li>
							 <li><i class="fa-li fa fa-half fa-circle"></i> Item 2</li>
							 <li><i class="fa-li fa fa-half fa-circle"></i> Item 3</li>
						 </ul>
						 <p><a href="./current"><i class="fa fa-angle-double-right"> </i> Click for Current Listings </a></p>
					</div>
		        </div>
		        <div class="col-lg-7 col-md-6 col-sm-5"></div>
		    </div>
		</div>
		<footer class="footer">
			<p>
				<a href="../jobs/">Jobs</a>
				<a href="../legal/">Legal</a>
				<a href="../privacy/">Privacy</a>
			</p>
		</footer>
		</div>
		</div>

	</body>
	<script>
		var img = new Image();
		img.onload = function() {
		$(".mask").addClass("clear");
			};
		img.src = "../public/img/jobs.jpg";
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</html>
