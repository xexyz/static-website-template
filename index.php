<!doctype html>
	<head>
		<meta charset="utf-8" />
		<title>COMPANY NAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../assets/lettertype.png" />
		<meta property="og:url" content="../" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<link rel="shortcut icon" href="../public/img/favicon.ico"/>
		<link rel="image_src" href="../assets/lettertype.png" />
		<link rel="stylesheet" href="../public/css/styles.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../public/js/jquery.tiles-gallery.js"></script>
		<link rel="stylesheet" href="../public/css/jquery-tilesgallery.css"/>
	</head>

	<body class="index">
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="navbar-header">
				<a class="brand" href="../"><img src="../public/img/lettertype.svg"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../PAGE1/">PAGE1</a></li>
					<li><a href="../PAGE2/">PAGE2</a></li>
					<li><a href="../PAGE3/">PAGE3</a></li>
					<li><a href="../contact/">CONTACT</a></li>
					<li><a href="https://LOGIN/">LOGIN</a></li>
				</ul>
			</div>
		</nav>
		<div id="index-bg" style='display:none;'>
			<?php
				$min = 2;
				$fols = array("b"=>8, "c"=>6, "d"=>10, "f"=>4, "m"=>4);
				foreach($fols as $x => $x_value) {
					$arrs = range(0,$x_value - 1);
					$suba = array_rand($arrs, rand($min, $x_value));
					foreach($suba as $y){
						$z = str_pad($y, 3, "0", STR_PAD_LEFT);
						echo "			<p id='$x$y'><img src='../public/img/x/$x/$z.jpg'/></p>";
					}
				}
			?>
		</div>
		<div id="logo">
			<img src="../public/img/logo.png"/>
		</div>
		<footer class="footer">
			<p>
				<a href="../jobs/">Jobs</a>
				<a href="../legal/">Legal</a>
				<a href="../privacy/">Privacy</a>
			</p>
		</footer>
	</body>
	<script>
		$("#index-bg").tilesGallery({
		   responsive: true,
		   margin: 2,
		});
		$("#index-bg").show();

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</html>
