<!doctype html>
	<head>
		<meta charset="utf-8" />
		<title>PAGE1 -- COMPANY NMAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME - PAGE1"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../assets/lettertype.png" />
		<meta property="og:url" content="../PAGE1/" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="shortcut icon" href="../public/img/favicon.ico"/>
		<link rel="image_src" href="../assets/lettertype.png" />
		<link rel="stylesheet" href="../public/css/styles.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	</head>

	<body class="PAGE1">
		<div class="cover">
		<div class="mask">
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="navbar-header">
				<a class="brand" href="../"><img src="../public/img/lettertype.svg"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="../PAGE1/">PAGE1</a></li>
					<li><a href="../PAGE2/">PAGE2</a></li>
					<li><a href="../PAGE3/">PAGE3</a></li>
					<li><a href="../contact/">CONTACT</a></li>
					<li><a href="https://LOGIN/">LOGIN</a></li>
				</ul>
			</div>
		</nav>
		<div class="container-fluid">
		    <div class="row">
		        <div class="col-lg-6 col-md-5 col-sm-5"></div>
		        <div class="col-lg-5 col-md-7 col-sm-7">
		            <div class="section textlight">
			        <h1 class="headline"> PAGE1 </h1>
						<p><i class="fa fa-info-circle"> </i> Nulla et molestie dolor. Nam tempor quam lacus, ac commodo nibh aliquam sed. Sed pharetra maximus gravida. Morbi sagittis sem sit amet lacus tristique, non porttitor lectus commodo. Suspendisse a vestibulum elit, non vestibulum dolor. Nunc diam nisi, maximus ac nisl id, posuere mattis eros. Etiam mollis mauris dapibus sapien blandit, sit amet varius dolor laoreet. Pellentesque sed ipsum sapien. Vestibulum et dapibus lorem. Vivamus tempus lorem ac sapien vulputate sagittis nec nec elit. Duis sagittis aliquet nulla vitae rhoncus. Donec vel dui nisl. Pellentesque dui risus, luctus eget mi et, laoreet porttitor arcu. Aenean leo orci, sodales vitae lorem non, commodo efficitur libero.</p>
						<p><i class="fa fa-group"> </i> Nulla et molestie dolor. Nam tempor quam lacus, ac commodo nibh aliquam sed. Sed pharetra maximus gravida. Morbi sagittis sem sit amet lacus tristique, non porttitor lectus commodo. Suspendisse a vestibulum elit, non vestibulum dolor. Nunc diam nisi, maximus ac nisl id, posuere mattis eros. Etiam mollis mauris dapibus sapien blandit, sit amet varius dolor laoreet. Pellentesque sed ipsum sapien. Vestibulum et dapibus lorem. Vivamus tempus lorem ac sapien vulputate sagittis nec nec elit. Duis sagittis aliquet nulla vitae rhoncus. Donec vel dui nisl. Pellentesque dui risus, luctus eget mi et, laoreet porttitor arcu. Aenean leo orci, sodales vitae lorem non, commodo efficitur libero.</p>
						<p><i class="fa fa-briefcase"> </i> Nulla et molestie dolor. Nam tempor quam lacus, ac commodo nibh aliquam sed. Sed pharetra maximus gravida. Morbi sagittis sem sit amet lacus tristique, non porttitor lectus commodo. Suspendisse a vestibulum elit, non vestibulum dolor. Nunc diam nisi, maximus ac nisl id, posuere mattis eros. Etiam mollis mauris dapibus sapien blandit, sit amet varius dolor laoreet. Pellentesque sed ipsum sapien. Vestibulum et dapibus lorem. Vivamus tempus lorem ac sapien vulputate sagittis nec nec elit. Duis sagittis aliquet nulla vitae rhoncus. Donec vel dui nisl. Pellentesque dui risus, luctus eget mi et, laoreet porttitor arcu. Aenean leo orci, sodales vitae lorem non, commodo efficitur libero.</p>
						<ul class="fa-ul">
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 1</li>
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 2</li>
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 3</li>
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 4</li>
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 5</li>
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 6</li>
							<li><i class="fa-li fa fa-half fa-circle"></i> Item 7</li>
						</ul>
		            </div>
		        </div>
		        <div class="col-lg-1 hidden-md"></div>
		    </div>
		</div>
		<footer class="footer">
			<p>
				<a href="../jobs/">Jobs</a>
				<a href="../legal/">Legal</a>
				<a href="../privacy/">Privacy</a>
			</p>
		</footer>
		</div>
		</div>
	</body>
	<script>
		var img = new Image();
		img.onload = function() {
			$(".mask").addClass("clear");
		};
		img.src = "../public/img/COVERPAGE1.jpg";

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</html>
