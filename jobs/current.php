<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Current Jobs -- COMPANY NAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME - Jobs"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../../assets/lettertype.png" />
		<meta property="og:url" content="../../legal/" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="robots" content="NOINDEX, NOFOLLOW" />
		<link rel="shortcut icon" href="../../public/img/favicon.ico"/>
		<link rel="image_src" href="../../assets/lettertype.png" />
		<link rel="stylesheet" href="../../public/css/styles.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	</head>
<body class ="disclaim">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<a class="brand" href="../../"><img src="../../public/img/lettertype.svg"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="../../PAGE1/">PAGE1</a></li>
				<li><a href="../../PAGE2/">PAGE2</a></li>
				<li><a href="../../PAGE3/">PAGE3</a></li>
				<li><a href="../../contact/">CONTACT</a></li>
				<li><a href="https://LOGIN/">LOGIN</a></li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3"></div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h3>CURRENT POSITIONS</h3>
				<script type='text/javascript' charset='utf-8'>
					//SOMETHING FROM SOME SERVICE
				</script>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3"></div>
		</div>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>
