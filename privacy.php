<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Privacy -- COMPANY NAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME - Privacy"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../assets/lettertype.png" />
		<meta property="og:url" content="../privacy/" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="robots" content="NOINDEX, NOFOLLOW" />
		<link rel="shortcut icon" href="../public/img/favicon.ico"/>
		<link rel="image_src" href="../assets/lettertype.png" />
		<link rel="stylesheet" href="../public/css/styles.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	</head>
<body class ="disclaim">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<a class="brand" href="../"><img src="../public/img/lettertype.svg"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="../PAGE1/">PAGE1</a></li>
				<li><a href="../PAGE2/">PAGE2</a></li>
				<li><a href="../PAGE3/">PAGE3</a></li>
				<li><a href="../contact/">CONTACT</a></li>
				<li><a href="https://LOGIN/">LOGIN</a></li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2"></div>
			<div class="col-lg-8 col-md-8 col-sm-8">

<h3>PRIVACY POLICY</h3>
<h4>LEGAL STUFF</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed efficitur varius laoreet. Proin erat est, tincidunt sed tempus a, vestibulum id libero. Sed semper sem odio, quis gravida velit sodales vehicula. Suspendisse dapibus sollicitudin erat, et sodales leo ullamcorper a. Pellentesque placerat magna vel augue consequat, ac facilisis ipsum finibus. Pellentesque ut convallis enim. Morbi purus lacus, pulvinar et urna consectetur, suscipit pretium ex. Vivamus non eros eleifend, ultrices risus ac, fermentum nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc ac nisl rhoncus, finibus magna sit amet, euismod erat. Donec turpis magna, auctor eu diam quis, lacinia sagittis nisi. Ut lacus nulla, laoreet at lobortis a, accumsan et sapien. In lacinia mauris eget nisi condimentum, non luctus urna convallis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse a purus pharetra, tincidunt mauris sit amet, aliquet lacus. Vestibulum leo sapien, tempor at placerat in, porta vel tortor. </p>
<p>Nulla et molestie dolor. Nam tempor quam lacus, ac commodo nibh aliquam sed. Sed pharetra maximus gravida. Morbi sagittis sem sit amet lacus tristique, non porttitor lectus commodo. Suspendisse a vestibulum elit, non vestibulum dolor. Nunc diam nisi, maximus ac nisl id, posuere mattis eros. Etiam mollis mauris dapibus sapien blandit, sit amet varius dolor laoreet. Pellentesque sed ipsum sapien. Vestibulum et dapibus lorem. Vivamus tempus lorem ac sapien vulputate sagittis nec nec elit. Duis sagittis aliquet nulla vitae rhoncus. Donec vel dui nisl. Pellentesque dui risus, luctus eget mi et, laoreet porttitor arcu. Aenean leo orci, sodales vitae lorem non, commodo efficitur libero.</p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2"></div>
		</div>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>
