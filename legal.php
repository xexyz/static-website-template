<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Legal -- COMPANY NAME</title>
		<meta name="description" content="COMPANY DESCRIPTION" />
		<meta property="og:title" content="COMPANY NAME - Legal"/>
		<meta property="og:type" content="website"/>
		<meta property="og:image" content="../assets/lettertype.png" />
		<meta property="og:url" content="../legal/" />
		<meta property="og:description" content="COMPANY DESCRIPTION"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="robots" content="NOINDEX, NOFOLLOW" />
		<link rel="shortcut icon" href="../public/img/favicon.ico"/>
		<link rel="image_src" href="../assets/lettertype.png" />
		<link rel="stylesheet" href="../public/css/styles.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	</head>
<body class ="disclaim">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<a class="brand" href="../"><img src="../public/img/lettertype.svg"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="../PAGE1/">PAGE1</a></li>
				<li><a href="../PAGE2/">PAGE2</a></li>
				<li><a href="../PAGE3/">PAGE3</a></li>
				<li><a href="../contact/">CONTACT</a></li>
				<li><a href="https://LOGIN/">LOGIN</a></li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2"></div>
			<div class="col-lg-8 col-md-8 col-sm-8">

<h3>TERMS & CONDITIONS</h3>
<h4>SECTION 1</h4>
<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
<h4>SECTION 2</h4>
<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
<h4>SECTION 3</h4>
<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fermentum mauris aliquet, tempor turpis vel, venenatis arcu. Aenean diam mi, placerat sit amet imperdiet a, finibus iaculis massa. Curabitur maximus fermentum risus, at congue ex ullamcorper eu. Vivamus eget ornare turpis. In aliquam aliquet metus at bibendum. Nam vitae justo a diam laoreet vulputate. Sed sed est et odio consequat mollis a tincidunt magna. Maecenas eu mauris posuere, mattis tellus a, egestas ipsum. Integer ipsum leo, mattis eu leo at, fermentum egestas diam.</p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2"></div>
		</div>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'GA_VALUE', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>
